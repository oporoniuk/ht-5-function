// Теоретичні питання
// 1. Можна зробити function declaration абр function expression.
//function declaration :  function doAction (параметри фу-ії) {блок коду, що та фунція робить}. 
// Щоб викликати функцію ми просто пишемо назву фунції н.п.: doAction
// function expresion : спочатку створюємо змінну до якої запишемо функцію: const doAction2 = function doAction () {}
//Щоб викликати функцію ми просто пишемо назву змінної н.п.: doAction2


// 2. Оператор return повертає значення функції. 
//На приклад: 
//function summ (a, b, c) {
    //return a + b + c;
//}

// 3.Параметри фунції пишуться в дужках, при своренні функції. 
//Якщо ніякий аргумент (тобто зовнішня змінна) не надає параметру значення, то можна дати йому значення за замовчуванням.
// function summ (a, b, c) {
    //return a + b + c;
//}
// summ (3, 8, 2);



// Практичні завдання
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
function divide(a , b) {
    return a / b;
}
let particle = divide(6, 2);
console.log(particle);



// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

function SimpleCalculator() {
    let operation = prompt("Choose the operation: +, -, *, /");
    while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/"){
        operation = prompt ('Please type one of these options:"+", "-", "*", "/" ');
        console.log(operation);
    };

    let firstNumber = prompt('Enter the first value');
    while (isNaN(firstNumber)) {
        console.log( typeof firstNumber,isNaN(firstNumber));
        firstNumber = prompt("Your value is invalid. Please enter a number!");
    }
    let secondNumber = prompt('Enter the second value');
    while (isNaN(secondNumber) || (secondNumber === '0' && operation === "/")) {
        console.log( typeof secondNumber,isNaN(secondNumber));
        secondNumber = prompt("Your value is invalid. Please enter a number and/or make sure you are not dividing by 0!");
    }

    firstNumber = Number(firstNumber);
    secondNumber = Number(secondNumber);

    return Calculate(firstNumber, secondNumber, operation);
}

function Calculate(firstNumber, secondNumber, operation) {
    let result;

    switch (operation) {
        case '+':
        result = firstNumber + secondNumber;
        break;

        case '-':
        result = firstNumber - secondNumber; 
        break;

        case '*':
        result = firstNumber * secondNumber;
        break;

        case '/':
            result = firstNumber / secondNumber;
            break;

        default: 
            alert ('Unknown operation');
            return;
    }
    return result;
}

console.log(`The result is ${SimpleCalculator()}`);


// 3. Опціонально. Завдання:
let userNumber = Number(prompt("Please enter your number"));
while (isNaN(userNumber)) {
            console.log( typeof userNumber,isNaN(userNumber));
            userNumber = prompt("Your value is invalid. Please enter a number!");
        }

function factorial(userNumber) {
    let result = 1;
    for (let i = 1; i <= userNumber; i++) {
        result = result * i;
    }
    return result;
}
console.log(`${userNumber}! is ${factorial(userNumber)}`);
